FROM node:8


#create appdirecctory
RUN mkdir -p /home/nodedemo/app/node_modules

WORKDIR /home/nodedemo/app

#install app dependencies
COPY * ./

RUN npm install

COPY . .
EXPOSE 8080
CMD [ "node" , "app.js" ]


